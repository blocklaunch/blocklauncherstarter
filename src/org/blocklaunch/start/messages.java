package org.blocklaunch.start;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.blocklaunch.Link;

public class messages {

public static void showfailedinetmessage(){
	String text;
	if(!StartUtilBL.isonline("www.google.de")){
		text="Can not connect to the internet. Please ensure that you are connected to the internet.";
	}else{
		text="Can not connect to our servers. Try again later, please.";
	}

	final JFrame frame = new JFrame();
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
 
	Object[] options = {"Okay"};
	int n = JOptionPane.showOptionDialog(frame,
			text,
			"Error while searching internet",
			JOptionPane.ERROR_MESSAGE,
			JOptionPane.ERROR_MESSAGE,
			null,     //do not use a custom Icon
			options,  //the titles of buttons
			options[0]); //default button title
	System.out.println(String.valueOf(n));

}

public static void showoutdatedjava(){


	final JFrame frame = new JFrame();
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	String text = "Your Java is outdated, please download and install a newer version!";
 
	Object[] options = {"Download", "Cancel"};
	int n = JOptionPane.showOptionDialog(frame,
			text,
			"Outdated Java",
			JOptionPane.ERROR_MESSAGE,
			JOptionPane.ERROR_MESSAGE,
			null,     //do not use a custom Icon
			options,  //the titles of buttons
			options[0]); //default button title
	System.out.println(String.valueOf(n));
	if (n == 0){
		StartUtilBL.visitsite(Link.Java_update);
	}

}

}
