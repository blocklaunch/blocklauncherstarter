/*
*  @author gelber_kaktus
*  This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License. To view a copy of the license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed
*  This programm is using JUPAR. http://masterex.github.io/archive/2011/12/25/jupar.html
*/

package org.blocklaunch.start;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;

import org.blocklaunch.Link;

import jupar.Downloader;

public class Start {
    Locale currentLocale;
    File workdir;
    static Integer thisversion = 6;
    
    public static void main(String[]args)
    {
    	System.out.println(System.getProperty("java.version"));
    	if(System.getProperty("java.version").startsWith("1.6")){
    		messages.showoutdatedjava();
    	}else{
    		try{
    			Start.start();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} 
    	}
    }

    public static void start() throws IOException{  
    	long zeitbegin = System.currentTimeMillis();
    	if(!StartUtilBL.checkStarterUpdate(thisversion)){
    		URL dllauncher = new URL(StartUtilBL.getDLserver(), "versions/BLauncher.jar");
    		System.out.println(dllauncher);
    		File workdir = Util.getWorkingDir();
    		File launcher = new File(workdir, "BLauncher.jar");
    		File launcherlib = new File(new File(workdir, "libraries"), "launcher_lib.jar");
    		if((!launcher.exists()||!launcherlib.exists())&&(!StartUtilBL.isonline(Link.URL))){
    			messages.showfailedinetmessage(); 
    		}else{
    			Integer current = 0;
    			String listedcurrent = Util.getProperties("current");
    			//System.out.println("4"+listedcurrent);
    			if (listedcurrent != null){
    				current = Integer.parseInt(listedcurrent);
    			}
    			boolean Dload = false;
    			if (launcher.exists()&&launcherlib.exists()){
    				Dload = StartUtilBL.checkUpdate(current); 
    			}else{ Dload = true;}
    			//dloaden
    			if (Dload){
    				System.out.println("Download...");
    				Downloader dl = new Downloader();
    				dl.wget(dllauncher, launcher.getAbsolutePath());
    				//getting lib aka mojangs launcher
    				Lib.getLib(workdir);
    			}

    			Desktop dr = Desktop.getDesktop();
    			long needtime = System.currentTimeMillis()-zeitbegin;
    			System.out.println(needtime);
    			if (needtime<5000){ try { Thread.sleep(5000-needtime);} catch (InterruptedException e) { e.printStackTrace();}}
    			dr.open(launcher);
    		}
    	}
    }
}

