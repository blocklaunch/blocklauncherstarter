package org.blocklaunch.start;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarOutputStream;
import java.util.jar.Pack200;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import jupar.Downloader;

import org.blocklaunch.Link;

import LZMA.LzmaInputStream;

//1 load official to lib folder //check
//2 remove classes				//check
//3 META-INF-BlockLauncher		//todo

public class Lib {
	private static File dir;
	private static ArrayList<String> notcopyfiles = StartUtilBL.getNonCopy(Link.Noncopy);/*new String[]{"GameLauncher", "Launcher", "OperatingSystem",
													"updater/LocalVersionList", "updater/RemoteVersionList",
													"updater/VersionList", "updater/VersionManager"}; */

	public static void getLib(File workdir) throws IOException{
		dir = new File(workdir, "libraries");

		Downloader dl = new Downloader();
		File lzmapacked = new File(dir, "launcher.pack.lzma");
		dl.wget(new URL(Link.officiallauncher_DL), lzmapacked.getAbsolutePath());
		//.pack file
		File packed = new File(dir, "launcher.pack");
		//
		File launcher = new File(dir, "launcher_lib.jar");
		try {
			decodelzma(lzmapacked, packed);
				lzmapacked.delete();
			depack(packed, launcher);
				packed.delete();
			cutoff(launcher);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void decodelzma(File infile, File outfile) throws Exception{
		System.out.println("Decoding LZMA from "+infile.getName() + " to " + outfile.getName());
		InputStream inputstream = null;
		OutputStream outputstream = null;
		try{
			inputstream = new LzmaInputStream(new FileInputStream(infile));
			outputstream = new FileOutputStream(outfile);
			
			byte[]buffer = new byte[65536];
		
			int read = inputstream.read(buffer);
		
			while (read >=1){
				outputstream.write(buffer, 0, read);
				read = inputstream.read(buffer);
			}
		}catch(Exception e){
			System.out.println("Decoding failed");
			e.printStackTrace();
		}finally{
			try{
				inputstream.close();
				outputstream.close();
			}catch(Exception e){}
			System.out.println("finished decoding");
		}
	}
	
	private static void depack(File packed, File launcher) {
		System.out.println("Depack "+packed.getName()+" to "+launcher.getName());
		JarOutputStream output = null;
		try{
			output = new JarOutputStream(new FileOutputStream(launcher));
			Pack200.newUnpacker().unpack(packed, output);
		}catch(Exception e){
			System.out.println("Depacking failed");
			e.printStackTrace();
		}finally{
			try{
				output.close();
			}catch(Exception e){}
			System.out.println("Depacking finished");
		}
		
	}
	
	private static void cutoff(File launcher)throws Exception{
		
		File temp = new File(dir, "launcher_lib_temp.jar");
		launcher.renameTo(temp);
		
		ZipFile input = null;
		ZipOutputStream output = null;
		
		try{
			input = new ZipFile(temp);
			output = new ZipOutputStream(new FileOutputStream(launcher));
			//auslesen+l�schen
			Enumeration<? extends ZipEntry> entries = input.entries();
			while(entries.hasMoreElements()){
				ZipEntry entry = null;
				try{
					entry = entries.nextElement();
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(entry != null && entry.getName()!= null){
					boolean copy = true;
					String name = entry.getName();
				
						System.out.println(name);
//					if(name.startsWith("net/minecraft/launcher/")){name = name.substring(23);}
//					
//					
//					if((name.startsWith("ui")||name.startsWith("profile")||name.startsWith("META"))){copy = false;}
//					else{
					for(int i=0; i<notcopyfiles.size(); i++){
						if (name.startsWith(notcopyfiles.get(i))){copy = false;}
					}
//					}
					if(copy){
//						System.out.println(name);
						output.putNextEntry(new ZipEntry(entry.getName()));
						if(!entry.isDirectory()){
							copy(input.getInputStream(entry), output);
						}
					}
				}
				output.closeEntry();
				
			}
			
			
		}finally{
			try{
				input.close();
				output.close();
				temp.delete();
			}catch(Exception e){}
		}
		
	}
	
	public static void copy(InputStream input, OutputStream output)
			throws IOException {
		byte[] BUFFER = new byte[4096 * 1024];
		int bytesRead;
		while ((bytesRead = input.read(BUFFER)) != -1) {
			output.write(BUFFER, 0, bytesRead);
		}
	}

}
