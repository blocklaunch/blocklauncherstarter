package org.blocklaunch.start;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.blocklaunch.Link;

public class StartUtilBL {
	
	public static boolean checkUpdate(Integer current){
		Integer version = null;
		String dummy = null;
		
		try
	       {            
	          URL url = new URL(Link.UpdateFile);
	        //  URLConnection connect = url.openConnection();
	          InputStream is = url.openStream();
	        //  BufferedReader br = new BufferedReader( new FileReader(is.toString()) );
	          BufferedReader br = new BufferedReader(new InputStreamReader(is));
	          
	          while ((dummy = br.readLine()) != null){
	        	  if (!dummy.equals("")){ 
	             version = Integer.parseInt(dummy);}
	          }
	          System.out.println(version);
	       }
	       catch(Exception ex)
	       {
	          //ex.printStackTrace();
	    	  version = 0;
	       }

	    System.out.println("This:"+String.valueOf(current));
	    System.out.println("online:"+String.valueOf(version));
	    	    	    
	    if (current>=version) {
	    	System.out.println("No Update");    	
	    	return false;
	    }else{
	    	System.out.println("Update");	    	
	    	return true;
	    }
	}
	
	public static ArrayList<String> getNonCopy(String fileurl){
        ArrayList<String> text = new ArrayList<String>(); // mit arraylist austauschen f�r gr��ere dateien
        String dummy = null;

       try
       {            
          URL url = new URL(fileurl);
        //  URLConnection connect = url.openConnection();
          InputStream is = url.openStream();
        //  BufferedReader br = new BufferedReader( new FileReader(is.toString()) );
          BufferedReader br = new BufferedReader(new InputStreamReader(is));

          
          while ((dummy = br.readLine()) != null){
        	  if (!(dummy.startsWith("#")&&dummy.equals(""))){ 
             text.add(dummy);}
          }
       }
       catch(Exception ex)
       {
          ex.printStackTrace();
       }

       return text;
    }
	
	
	public static void visitsite(String site){
	    URL url = null;
        try {
            url = new URL(site);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if(Desktop.isDesktopSupported()){
            try {
                Desktop.getDesktop().browse(url.toURI());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
	}
	
	public static boolean checkStarterUpdate(Integer current){
		Integer version = null;
		String dummy = null;
		
		try
	       {            
	          URL url = new URL(Link.UpdateFile_starter);
	        //  URLConnection connect = url.openConnection();
	          InputStream is = url.openStream();
	        //  BufferedReader br = new BufferedReader( new FileReader(is.toString()) );
	          BufferedReader br = new BufferedReader(new InputStreamReader(is));
	          
	          while ((dummy = br.readLine()) != null){
	        	  if (!dummy.equals("")){ 
	             version = Integer.parseInt(dummy);}
	          }
	          System.out.println(version);
	       }
	       catch(Exception ex)
	       {
	          //ex.printStackTrace();
	    	  version = 0;
	       }

	    System.out.println("This starter:"+String.valueOf(current));
	    System.out.println("online starter:"+String.valueOf(version));
	    	    	    
	    if (current>=version) {
	    	System.out.println("No Update");
	    	return false;
	    }else{
	    	boolean retur = false;
	    	final JFrame frame = new JFrame();
	    	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    	System.out.println("Update");	    	
	    	Object[] options = {"Yes, visit download site", "No, thanks"};
        	int n = JOptionPane.showOptionDialog(frame,
        		    "An update is available, \n would you like to download",
        		    "Update",
        		    JOptionPane.YES_NO_OPTION,
        		    JOptionPane.PLAIN_MESSAGE,
        		    null,     //do not use a custom Icon
        		    options,  //the titles of buttons
        		    options[0]); //default button title
        	System.out.println(String.valueOf(n));
        	//yes (0) > updateseite
        	if (n == 0){
        		StartUtilBL.visitsite(Link.UpdateSite);
        		retur = true;
        	}
        	return retur;
	    }
	}

	public static URL getDLserver() throws MalformedURLException{
			URL back = new URL(Link.SERVER);
			return back;
    }
	
	public static boolean isonline(String host){
		//String host = Link.SERVER;
		System.out.println("Pinging "+host);
		
		Socket socket = null;
		boolean reachable = false;
		try {
			try{
		    socket = new Socket(host, 80);
		    reachable = true;		
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();				}
		} finally {            
		    if (socket != null) try { socket.close(); } catch(IOException e) {}
	
		}
		System.out.println(String.valueOf(reachable));
		return reachable;
	}
}	

