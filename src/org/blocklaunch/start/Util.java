package org.blocklaunch.start;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class Util {
    public static final String APPLICATION_NAME = "blocklaunch";
    public static String propFileLocation = Util.getStandardWorkingDirectory() + "/BLauncher.properties";
    public static File propFile = new File(propFileLocation);
    public static Properties prop = new Properties();

    public static String getProperties(String key) {
        String value = "";
        Properties prop = new Properties();
        if (!propFile.exists()) {        	
            try {
				propFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }         
            try {
            	prop.load(new FileInputStream(propFile));
            } catch (IOException ex) {
                Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
            }
            value = prop.getProperty(key);       

        return value;
    }  
    

    public static Object setProperties(String key, String value) {
        Object finalValue = "";
        System.out.println(propFile.getAbsolutePath());
        if (!propFile.exists()) {
            return finalValue;
        } else {
            try {
                prop.load(new FileInputStream(propFile));
            } catch (IOException ex) {
                Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
            }
            finalValue = prop.setProperty(key, value);
            try {
                prop.store(new FileOutputStream(propFile), "BLauncher");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return finalValue;
    }

    public static void restartApplication() throws URISyntaxException, IOException {
        final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        final File currentJar = new File(Start.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        System.out.println(currentJar.getAbsolutePath());
  /* is it a jar file? */
        if (currentJar.getName().endsWith("exe")) {

            final ArrayList<String> command = new ArrayList<String>();
            // command.add(javaBin);
            //command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
            System.exit(0);
        }

        if (!currentJar.getName().endsWith(".jar"))
            return;

  
        final ArrayList<String> command = new ArrayList<String>();
        command.add(javaBin);
        command.add("-jar");
        command.add(currentJar.getPath());

        final ProcessBuilder builder = new ProcessBuilder(command);
        builder.start();
        System.exit(0);
    }

    public static OS getPlatform() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) return OS.WINDOWS;
        if (osName.contains("mac")) return OS.MACOS;
        if (osName.contains("linux")) return OS.LINUX;
        if (osName.contains("unix")) return OS.LINUX;
        return OS.UNKNOWN;
    }

    public static File getStandardWorkingDirectory() {
        String userHome = System.getProperty("user.home", ".");
        File workingDirectory;
        OS os = getPlatform();
        System.out.println(os.ordinal());
        if (os == OS.LINUX)
        	{
            workingDirectory = new File(userHome, ".blocklaunch/");
        	}
        else{
        	if (os == OS.WINDOWS)
        	{
        		String applicationData = System.getenv("APPDATA");
        		String folder = applicationData != null ? applicationData : userHome;
        		workingDirectory = new File(folder, ".blocklaunch/");
        	}else{
        		if (os == OS.MACOS){
        			workingDirectory = new File(userHome, "Library/Application Support/blocklaunch");
        		}else{
        			workingDirectory = new File(userHome, "blocklaunch/");}
        		}
        	} 
        
        return workingDirectory;
    }


    public static enum OS {
        WINDOWS, MACOS, SOLARIS, LINUX, UNKNOWN;
    }
    
    public static boolean stringHasValue(String string) {
        return (string != null) && (!string.isEmpty());
    }
    
    public static File getWorkingDir(){
    	String workdirs;
    	File workdir;
    	workdirs = Util.getProperties("installation_dir");
    	System.out.println(workdirs);
    	if (workdirs == null||workdirs.isEmpty()){
    		System.out.println("1a");
    		workdir = Util.changeDir();
    	}else{    	
    		System.out.println("1b");
    	workdir = new File(workdirs);}
    	return workdir;
    }
    
    public static File changeDir(){
    	JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(1);
        fileChooser.setCurrentDirectory(Util.getStandardWorkingDirectory());
        fileChooser.setDialogTitle("Choose your BlockLaunch installation directory : (Cancelling will choose the default one)");
        
        File returnes = Util.getStandardWorkingDirectory();
        
        int ret = fileChooser.showOpenDialog(fileChooser);
        if (ret == 0) {
            File dir = fileChooser.getSelectedFile();
            prop.setProperty("installation_dir", dir.getAbsolutePath());
            returnes = dir;
        } else if (ret == 1) {
            prop.setProperty("installation_dir", Util.getStandardWorkingDirectory().getAbsolutePath());           
        }
        try {
            prop.store(new FileOutputStream(propFile), "BLauncher");
        } catch (IOException ex) {
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }
        return returnes;
    }
    
    
    public static final HyperlinkListener EXTERNAL_HYPERLINK_LISTENER = new HyperlinkListener() {
		public void hyperlinkUpdate(HyperlinkEvent paramAnonymousHyperlinkEvent) {
			if (paramAnonymousHyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
				try {
					openLink(paramAnonymousHyperlinkEvent.getURL().toURI());
				} catch (Exception localException) {
					localException.printStackTrace();
				}
		}
	};
	
	public static void openLink(URI paramURI) {
		try {
			Object localObject = Class.forName("java.awt.Desktop")
					.getMethod("getDesktop", new Class[0])
					.invoke(null, new Object[0]);
			localObject.getClass()
					.getMethod("browse", new Class[] { URI.class })
					.invoke(localObject, new Object[] { paramURI });
		} catch (Throwable localThrowable) {
			System.out.println("Failed to open link " + paramURI.toString());
		}
	}
}
